package com.example.demo.controller;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comments;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentsService;
import com.example.demo.service.ReportService;


@Controller
public class ForumController {

	@Autowired
	ReportService reportService;

	@Autowired
	CommentsService commentsService;

	// 投稿内容表示画面
	@GetMapping
	public ModelAndView top(@RequestParam(value = "start", required = false) String start, @RequestParam(value = "end", required = false) String end) {
	ModelAndView mav = new ModelAndView();


	//日付で絞り込み
	List<Report> contentData = null;
//	List<Comments> commentsData = null;
	Timestamp Beginning = Timestamp.valueOf("2020-01-01 00:00:00");;
	Timestamp finish = new Timestamp(System.currentTimeMillis());

	//もしカレンダーの始まりと終わりの欄が空欄じゃなかったら
	if(!ObjectUtils.isEmpty(start)) {
		Beginning = Timestamp.valueOf(start + " 00:00:00");
	}
	if(!ObjectUtils.isEmpty(end)) {
		finish = Timestamp.valueOf(end + " 23:59:59");
	}



	//絞り込んで取得のパターンと、全件取得のパターンで作る
	if(!ObjectUtils.isEmpty(start) || !ObjectUtils.isEmpty(end)) {
		contentData = reportService.findAllReport(Beginning, finish);
	} else {
		contentData = reportService.findAllReport();
	}


//	// 投稿を全件取得
//	List<Report> contentData = reportService.findAllReport();

	// コメント内容を全件取得
	List<Comments> commentsData = commentsService.findAllReport();

	// 画面遷移先を指定
	mav.setViewName("/top");
	// 投稿データオブジェクトを保管
	mav.addObject("contents", contentData);

	// コメント投稿データオブジェクトを保管
	mav.addObject("comments", commentsData);

	return mav;
	}


	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
	ModelAndView mav = new ModelAndView();

	// form用の空のentityを準備
	Report report = new Report();

	// 画面遷移先を指定
	mav.setViewName("/new");

	// 準備した空のentityを保管
	mav.addObject("formModel", report);
	return mav;
	}


	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		// 投稿をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}


	//削除ボタン追加
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		reportService.deleteReport(id);
		return new ModelAndView("redirect:/");
	}


	//編集画面追加（画面遷移＆コメント表示）
	@GetMapping("/edit/{id}")
		public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();

		Report report = reportService.findReport(id);
		mav.addObject("formModel", report);
		mav.setViewName("/edit");
		return mav;
	}


	//編集画面追加（編集内容登録）
	@PutMapping("/update/{id}")
		public ModelAndView updateContent(@PathVariable Integer id, @ModelAttribute("formModel") Report report) {

		report.setId(id);
		report.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
		reportService.saveReport(report);
		return new ModelAndView("redirect:/");
	}

	//コメント投稿処理

//	@Autowired
//	CommentsService commentsService;
//
//		// コメント内容表示画面
//		@GetMapping
//		public ModelAndView to() {
//		ModelAndView mav = new ModelAndView();
//
//		// コメント内容を全件取得
//		List<Comments> commentsData = commentsService.findAllReport();
//
//		// 画面遷移先を指定
//		mav.setViewName("/top");
//		// コメント投稿データオブジェクトを保管
//		mav.addObject("comments", commentsData);
//		return mav;
//		}

	//コメント投稿処理
	@PostMapping("/reply")
	public ModelAndView addComments(@ModelAttribute("formModel") Comments comments) {
		// コメント投稿をテーブルに格納
		commentsService.saveComments(comments);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//コメント削除
	@DeleteMapping("/deleteComments/{id}")
	public ModelAndView deleteComments(@PathVariable Integer id) {
		commentsService.deleteComments(id);
		return new ModelAndView("redirect:/");
	}

	//コメント編集画面追加（画面遷移＆コメント表示）
		@GetMapping("/editComments/{id}")
			public ModelAndView editComments(@PathVariable Integer id) {
			ModelAndView mav = new ModelAndView();
			Comments comments = commentsService.findComments(id);
			mav.addObject("formModel", comments);
			mav.setViewName("/editComments");
			return mav;
		}

		//編集画面追加（編集内容登録）
		@PutMapping("/updateComments/{id}")
			public ModelAndView updateComments(@PathVariable Integer id, @ModelAttribute("formModel") Comments comments) {
			comments.setId(id);

            comments.setUpdatedDate(new Timestamp(System.currentTimeMillis()));

//			System.out.println(comments.getReportId());

			commentsService.saveComments(comments);
			return new ModelAndView("redirect:/");
		}



		 @PostMapping("/test")
		    @ResponseBody
		    public String note(@RequestParam String note) {
		        return note;
		    }

}
