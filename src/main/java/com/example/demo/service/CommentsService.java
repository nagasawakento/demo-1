package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comments;
import com.example.demo.repository.CommentsRepository;


@Service
public class CommentsService {


	@Autowired
	CommentsRepository commentsRepository;

	// レコード全件取得、   ByOrderByUpdatedDateDescで更新日時の新しい順で獲得
	public List<Comments> findAllReport() {
	return commentsRepository.findAllByOrderByUpdatedDateDesc();
	}
	// レコード追加
	public void saveComments(Comments Comments) {
		commentsRepository.save(Comments);
	}

	//削除ボタン
	public void deleteComments(Integer id) {
		commentsRepository.deleteById(id);
	}

	//編集機能追加
		public Comments findComments(Integer id) {
			Comments comments = commentsRepository.findById(id).orElse(null);
			return comments;
		}

}
