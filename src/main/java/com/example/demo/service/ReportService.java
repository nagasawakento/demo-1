package com.example.demo.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;


@Service
public class ReportService {

	@Autowired
	ReportRepository reportRepository;

	// レコード全件取得、
	public List<Report> findAllReport() {
	return reportRepository.findAllByOrderByUpdatedDateDesc();
	}


	//日付絞り込みをしたパターン
	public List<Report> findAllReport(Timestamp Beginning, Timestamp finish) {
	return reportRepository.findByCreatedDateBetween(Beginning, finish);
	}


	// レコード追加
	public void saveReport(Report report) {
		reportRepository.save(report);
	}


	//削除ボタン
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}


	//編集機能追加
	public Report findReport(Integer id) {
		Report report = reportRepository.findById(id).orElse(null);
		return report;
	}


}
