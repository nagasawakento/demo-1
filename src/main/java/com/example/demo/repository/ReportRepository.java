package com.example.demo.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Report;
@Repository
public interface ReportRepository extends JpaRepository<Report, Integer> {

	List<Report> findAllByOrderByUpdatedDateDesc();

	List<Report> findByCreatedDateBetween(Timestamp beginning, Timestamp finish);

}